name := "koviryator"

version := "0.1"

scalaVersion := "2.12.4"

// https://mvnrepository.com/artifact/org.scala-lang/scala-library
libraryDependencies += "org.scala-lang" % "scala-library" % "2.12.4"
// https://mvnrepository.com/artifact/org.scala-lang.modules/scala-swing
libraryDependencies += "org.scala-lang.modules" %% "scala-swing" % "2.0.1"
libraryDependencies += "org.scalafx" % "scalafx_2.12" % "8.0.144-R12"

// https://mvnrepository.com/artifact/com.github.pathikrit/better-files
libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.4.0"
libraryDependencies += "com.typesafe.slick" %% "slick" % "3.2.0"
libraryDependencies += "com.h2database" % "h2" % "1.4.185"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.2"
// https://mvnrepository.com/artifact/org.postgresql/postgresql
libraryDependencies += "org.postgresql" % "postgresql" % "42.2.2"


assemblyJarName in assembly := "kov.jar"
mainClass in assembly := Some("Main")