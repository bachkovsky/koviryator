package components

import model.LogEvent._
import utils.TimeUtils._

import scala.collection.JavaConverters._
import scala.util.Random
import scalafx.Includes.when
import scalafx.beans.property.ObjectProperty
import scalafx.scene.layout.Region
import scalafx.scene.text.{Font, Text, TextFlow}

class LogFlow extends TextFlow {

  prefWidth = Region.USE_PREF_SIZE

  val selectedFilter: ObjectProperty[Option[Column[_]]] = ObjectProperty(None)

  def display(page: Page): Unit = {
    children.clear()
    for (event <- page) {
      children.asScala +=
        clickable(event.thread, fillColor = Hash) +=
        text("#") +=
        clickable(event.host) +=
        text(" ") +=
        text(event.timestamp.value.formatted) +=
        text(" [") +=
        clickable(event.url) +=
        text(" ") +=
        clickable(event.user, fillColor = Hash) +=
        text(" ") +=
        clickable(event.ip, fillColor = Hash) +=
        text("] ") +=
        clickable(event.level, fillColor = colorOf(event.level)) +=
        text("(") +=
        clickable(event.logger, fillColor = Hash) +=
        text("): ") +=
        text(event.sysEvent) +=
        text("\n")
    }
  }

  import scalafx.scene.paint.Color

  private def colorOf(level: Level) = Fixed(
    level match {
      case Debug => Color.rgb(23, 127, 14)
      case Info  => Color.rgb(191, 172, 79)
      case Warn  => Color.Orange
      case Error => Color.Red
    }
  )

  private sealed trait Colored
  private object Hash extends Colored
  private case class Fixed(color: Color) extends Colored

  private def clickable(attr: Column[_], fillColor: Colored = Fixed(Color.Black)) = {
    new Text(attr.value.toString) {
      fill = fillColor match {
        case Hash =>
          val rand = new Random(text.value.hashCode)
          Color.rgb(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255))
        case Fixed(color) => color
      }

      private val blue: javafx.scene.paint.Paint = Color.Blue.delegate

      fill <== when(hover) choose blue otherwise fill()
      underline <== hover

      onMouseClicked = _ => selectedFilter() = Some(attr)
      font = Font.font("Monospaced")
    }
  }

  private def text(value: String) = new Text(value) {
    font = Font.font("Monospaced")
  }
}
