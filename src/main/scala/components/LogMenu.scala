package components

import java.io.File

import scalafx.scene.control.{Menu, MenuBar, MenuItem}
import scalafx.stage.FileChooser.ExtensionFilter
import scalafx.stage.{DirectoryChooser, FileChooser}
import scala.collection.JavaConverters._
import scalafx.beans.property.ObjectProperty

class LogMenu extends MenuBar {
  val selectedFiles: ObjectProperty[Seq[File]] = ObjectProperty(List.empty)

  menus = Seq(
    new Menu("_File") {
      mnemonicParsing = true
      items = Seq(
        new MenuItem("Open File") {
          onAction = {
            _ =>
              val fileChooser = new FileChooser {
                title = "Select Log Files"
                extensionFilters.asScala ++= Seq(
                  new ExtensionFilter("Log Files", "System*.log"),
                  new ExtensionFilter("Log Archives", "System*.zip")
                )
              }

              val files = fileChooser.showOpenMultipleDialog(null)
              if (files != null) {
                selectedFiles() = files
              }
          }
        },
        new MenuItem("Open Directory") {
          onAction = _ => {
            val chooser = new DirectoryChooser {
              title = "Select directory"
            }

            val dir = chooser.showDialog(null)
            if (dir != null) {
              selectedFiles() = dir.listFiles
            }
          }
        }
      )
    }
  )
}
