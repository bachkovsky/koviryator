package components

import java.io

import model.LogEvent.{Column, Page}
import parser.ParserComponent
import service.EventRepositoryComponent
import utils.Futures

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}
import scalafx.application.JFXApp.PrimaryStage
import scalafx.application.Platform.runLater
import scalafx.scene.Scene
import scalafx.scene.control.ScrollPane.ScrollBarPolicy
import scalafx.scene.control.{ScrollPane, TextField}
import scalafx.scene.layout.BorderPane

trait UiRepositoryComponent {
  this: EventRepositoryComponent with ParserComponent =>

  def ui: UiRepository

  final class UiRepository {
    lazy val logFlow = new LogFlow {
      selectedFilter.onChange { (_, oldFilter, newFilter) =>  if (oldFilter != newFilter) { updateView(newFilter) }}
    }

    lazy val menu = new LogMenu {
      selectedFiles.onChange { (_, _, newFiles) => saveAndDisplay(newFiles) }
    }

    lazy val mainStage = new PrimaryStage {
      title = "Ковырятор"
      scene = new Scene {
        root = new BorderPane {
          top = menu

          center = new ScrollPane() {
            content = logFlow
            hbarPolicy = ScrollBarPolicy.Never
            fitToWidth = true

            logFlow.selectedFilter.onChange(scrollToTop())

            private def scrollToTop(): Unit = {
              vvalue() = 0.0d
            }
          }

          bottom = new TextField {
            promptText = "input select query here"

            onAction = _ => {
              display(events.byQuery(text()))
            }
          }
        }
      }
    }

    private def display(futurePage: Future[Page]): Unit = {
      futurePage onComplete {
        case Success(page) => runLater(logFlow.display(page))
        case Failure(err) => err.printStackTrace()
      }
    }

    private def saveAndDisplay(files: Seq[io.File]): Unit = {
      events.clear() andThen {
        case _ =>
          val toInsert = parser.parseEvents(files).map(_.flatMap(events.insert))
          Futures.firstSuccessful(toInsert) onComplete { _ => display(events.top) }
      }
    }

    private def updateView(filter: Option[Column[_]]): Unit = {
      val page = filter.map(events.list).getOrElse(events.top)
      display(page)
    }
  }
}
