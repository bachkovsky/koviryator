package utils

import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.time.{Instant, LocalDateTime, ZoneId, ZonedDateTime}

import model.LogEvent.Timestamp

object TimeUtils {
  val formatter: DateTimeFormatter =
    DateTimeFormatter
      .ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
      .withZone(ZoneId.systemDefault())

  implicit class TemporalOps(temp: TemporalAccessor) {
    def toInstant: Instant =
      Instant.from(
        ZonedDateTime.of(
          LocalDateTime.from(temp),
          ZoneId.systemDefault
        )
      )

    def formatted: String = formatter.format(temp)
  }

  implicit val timestampOrdering: Ordering[Timestamp] = Ordering.by(_.value)
}
