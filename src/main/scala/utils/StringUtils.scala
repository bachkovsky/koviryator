package utils

object StringUtils {
  implicit class StringUtils(s: String) {
    def opt: Option[String] = if (s.isEmpty) None else Some(s)
  }
}
