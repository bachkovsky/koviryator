package utils

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success, Try}

object Futures {
  def firstSuccessful[T](fs: TraversableOnce[Future[T]])(implicit ex: ExecutionContext): Future[T] = {
    val p = Promise[T]()
    val count = new java.util.concurrent.atomic.AtomicInteger(fs.size)
    def bad() = if (count.decrementAndGet == 0) { p tryComplete Failure(new RuntimeException("All bad")) }
    val completeFirst: Try[T] => Unit = p tryComplete _
    fs foreach { _ onComplete { case v @ Success(_) => completeFirst(v) case _ => bad() }}
    p.future
  }
}
