package service

import model.LogEvent
import model.LogEvent._

import scala.concurrent.Future

trait EventRepositoryComponent {

  def events: EventRepository

  trait EventRepository {
    def byQuery(str: String): Future[Page]

    def clear(): Future[Int]

    /**
      * @param event event to insert
      * @return future id of inserted event
      */
    def insert(event: LogEvent): Future[Int]

    /**
      * @param events sequence of events to insert
      * @return number of rows affected, as returned by database
      */
    def insert(events: Iterable[LogEvent]): Future[Option[Int]]

    def threads: Future[Seq[Thread]]

    def top: Future[Page]

    def list(filter: Column[_]): Future[Page]
  }
}
