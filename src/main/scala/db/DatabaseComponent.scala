package db

import slick.dbio.DBIO

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

trait DatabaseComponent {
  this: Profile =>

  private[db] val db: profile.backend.Database

  implicit class QueryExecutor[T](program: DBIO[T]) {
    def exec: T = Await.result(db.run(program), 200 seconds)

    def run: Future[T] = db.run(program)
  }
}
