package db


trait PostgresDatabaseComponent extends DatabaseComponent with Profile {
  override val profile = slick.jdbc.PostgresProfile

  import profile.api._

  override val db = Database.forURL(
    "jdbc:postgresql://192.168.122.158:5432/logs",
    driver = "org.postgresql.Driver",
    keepAliveConnection = true,
    user = "ubuntu",
    password = "ubuntu"
  )
}
