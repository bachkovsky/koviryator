package db

import model.LogEvent
import model.LogEvent.{Host, Ip, Thread, Url, User, _}
import service.EventRepositoryComponent

import scala.concurrent.Future

trait SlickEventRepositoryComponent extends EventRepositoryComponent {
  this: DatabaseComponent with Tables with Profile =>

  override val events: EventRepository = new SlickEventRepository

  final class SlickEventRepository extends EventRepository {

    import profile.api._

    /**
      * @param event event to insert
      * @return future id of inserted event
      */
    override def insert(event: LogEvent): Future[Int] =
      (LogEvents += event).run

    /**
      * @param events sequence of events to insert
      * @return number of rows affected, as returned by database
      */
    override def insert(events: Iterable[LogEvent]): Future[Option[Int]] =
      (LogEvents ++= events).run

    override def threads: Future[Seq[Thread]] =
      LogEvents.map(_.thread).distinct.sorted.result.run

    override def top: Future[Page] =
      eventsQuery.take(200).result.run

    override def list(filterValue: Column[_]): Future[Page] = {
      val filterExpr: LogEventsTable => Rep[Boolean] = filterValue match {
        case thread: Thread       => _.thread === thread
        case user: User           => _.user === user
        case host: Host           => _.host === host
        case url: Url             => _.url === url
        case ip: Ip               => _.ip === ip
        case id: Id               => _.id === id
        case level: Level         => _.level === level
        case logger: Logger       => _.logger === logger
        case timestamp: Timestamp => _.timestamp === timestamp
      }
      byFilter(filterExpr)
    }

    private def byFilter(predicate: LogEventsTable => Rep[Boolean]) = {
      eventsQuery.filter(predicate).take(200).result.run
    }

    private def eventsQuery =
      LogEvents.sortBy(e => (e.timestamp, e.id))

    override def clear(): Future[Int] =
      LogEvents.delete.run

    override def byQuery(filter: String): Future[Page] = {
      sql"""select * from "events" where #$filter limit 200""".as[LogEvent].run
    }
  }
}
