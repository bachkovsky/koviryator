package db


trait H2DatabaseComponent extends DatabaseComponent with Profile {
  override val profile = slick.jdbc.H2Profile

  import profile.api._

  override val db = Database.forURL(
    "jdbc:h2:mem:events",
    driver = "org.h2.Driver",
    keepAliveConnection = true
  )
}