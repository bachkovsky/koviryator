package db

import java.time.Instant

import model.LogEvent
import model.LogEvent._
import slick.jdbc.GetResult

trait Tables {
  this:  DatabaseComponent with Profile =>

  import java.sql.{Timestamp => JTimestamp}

  import profile.api._

  implicit lazy val instantColumnType = MappedColumnType.base[Instant, JTimestamp](
    { instant => if (instant == null) null else new JTimestamp(instant.toEpochMilli) },
    { timestamp => if (timestamp == null) null else Instant.ofEpochMilli(timestamp.getTime) }
  )

  implicit lazy val levelColumnType = MappedColumnType.base[Level, String](
    _.toString,
    Level.withName
  )

  class LogEventsTable(tag: Tag) extends Table[LogEvent](tag, "events") {
    val id        = column[Id]("id", O.PrimaryKey, O.AutoInc)
    val thread    = column[Thread]("thread")
    val host      = column[Host]("host")
    val timestamp = column[Timestamp]("timestamp")
    val url       = column[Url]("url")
    val user      = column[User]("user")
    val ip        = column[Ip]("ip")
    val level     = column[Level]("level")
    val logger    = column[Logger]("logger")
    val sysEvent  = column[String]("sys_event")

    override def * =
      (id, thread, host, timestamp, url, user, ip, level, logger, sysEvent).mapTo[LogEvent]
  }

  private[db] implicit val GetLogEvent = GetResult[LogEvent](
    r =>
      LogEvent(
        Id(r.<<),
        Thread(r.<<),
        Host(r.<<),
        Timestamp(r.nextTimestamp().toInstant),
        Url(r.<<),
        User(r.<<),
        Ip(r.<<),
        Level.withName(r.<<),
        Logger(r.<<),
        r.<<
      )
  )

  lazy val LogEvents = TableQuery[LogEventsTable]

  def initSchema(): Unit = {
    LogEvents.schema.create.run
  }
}
