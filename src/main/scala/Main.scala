import scalafx.application.JFXApp

object Main extends JFXApp with AppComponents {
  initSchema()
  stage = ui.mainStage
}
