package model

import java.time.Instant

import model.LogEvent._
import slick.lifted.MappedTo
import utils.TimeUtils

final case class LogEvent(
    id: Id = Id(0L),
    thread: Thread,
    host: Host,
    timestamp: Timestamp,
    url: Url,
    user: User,
    ip: Ip,
    level: LogEvent.Level,
    logger: Logger,
    sysEvent: String
) {

  def format =
    s"${thread.value}#${host.value} ${TimeUtils.formatter.format(
      timestamp.value)} [${url.value} ${user.value} ${ip.value}] ${level.value.toUpperCase}(${logger.value}):$sysEvent"

}

object LogEvent {
  type Page = Seq[LogEvent]
  sealed trait Column[T] extends Any with MappedTo[T]

  case class Id(value: Long) extends AnyVal with Column[Long]
  case class Thread(value: String) extends AnyVal with Column[String]
  case class Host(value: String) extends AnyVal with Column[String]
  case class Timestamp(value: Instant) extends AnyVal with Column[Instant]
  case class Url(value: String) extends AnyVal with Column[String]
  case class User(value: String) extends AnyVal with Column[String]
  case class Ip(value: String) extends AnyVal with Column[String]
  case class Logger(value: String) extends AnyVal with Column[String]

  sealed trait Level extends Column[String] {
    override def value = toString
  }

  case object Debug extends Level
  case object Info extends Level
  case object Warn extends Level
  case object Error extends Level

  object Level {
    val values = List(Debug, Info, Warn, Error)

    def withName(name: String): Level = unapply(name).get

    def unapply(arg: String): Option[Level] =
      values.find(_.toString.equalsIgnoreCase(arg))
  }

  val tupled = (this.apply _).tupled
}
