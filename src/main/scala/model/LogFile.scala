package model

import java.time.Instant
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

import better.files.File
import utils.TimeUtils._

final case class LogFile(file: File) {

  private val formatter =
    new DateTimeFormatterBuilder()
      .appendPattern("yyyy-MM-dd_HH.mm.ss")
      .appendFraction(ChronoField.MILLI_OF_SECOND, 0, 3, true)
      .toFormatter

  lazy val timestamp: Instant = extractDate(file)

  private def extractDate(file: File): Instant = {
    if (!file.nameWithoutExtension.contains("_") || file.extension.contains(".zip")) {
      file.lastModifiedTime
    } else {
      val name = file.nameWithoutExtension(includeAll = false)
      val timestamp = name.substring(name.indexOf("_") + 1)
      formatter.parse(timestamp).toInstant
    }
  }
}
