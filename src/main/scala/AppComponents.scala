import components.UiRepositoryComponent
import db._
import parser.ParserComponent

trait AppComponents
    extends ParserComponent
    with SlickEventRepositoryComponent
    with UiRepositoryComponent
    with H2DatabaseComponent
    with Tables {
  val parser = new LogParser
  val ui = new UiRepository
}
