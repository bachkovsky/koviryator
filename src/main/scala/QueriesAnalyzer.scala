import java.time.Duration
import java.time.format.DateTimeFormatter

import db.{PostgresDatabaseComponent, SlickEventRepositoryComponent, Tables}
import model.LogEvent
import model.LogEvent.{Logger, Timestamp}
import utils.TimeUtils._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object QueriesAnalyzer
  extends App
    with SlickEventRepositoryComponent
    with PostgresDatabaseComponent
    with Tables {

  import profile.api._

  import scala.collection.JavaConversions._

  val formatter: DateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME

  val ev = LogEvents
    .filter(
      ev =>
        ev.timestamp < time("2018-03-29T09:46:59.00")
          && ev.timestamp > time("2018-03-29T09:45:00.00")
          && (ev.logger === Logger("DB") || ev.logger === Logger("DBSession")))
    .sortBy(e => (e.timestamp, e.id))
    .result
    .exec

  val TrStart = """(?s)\s+(\d+) begin.*""".r
  val TrEnd = """(?s)\s+(\d+) end.*""".r
  val TrExec = """(?s)\s+Execute SQL\((\d+)\).*""".r
  val TrSuccess = """(?s)\s+log successfully ends for session \+ (\d+).*""".r

  def getTrId(e: LogEvent) = e.sysEvent.value match {
    case TrStart(id) => id
    case TrEnd(id) => id
    case TrExec(id) => id
    case TrSuccess(id) => id
  }


  for (e <- ev.groupBy(_.thread).entrySet) {
    val thread: LogEvent.Thread = e.getKey
    val threadEvents: Seq[LogEvent] = e.getValue

    val events = threadEvents.dropWhile(e => !TrStart.pattern.matcher(e.sysEvent.value).matches())

    val transactions = new mutable.HashMap[String, ListBuffer[LogEvent]]()

    for (event <- events) {
       if (!hasNoTransaction(event)) {
         val trId = getTrId(event)
         val list: ListBuffer[LogEvent] = transactions.getOrDefault(trId, new ListBuffer[LogEvent]())
         list += event
         transactions.put(trId, list)
      }
    }

    for (e <- transactions.entrySet) {
      val trId = e.getKey
      val trans = e.getValue
      val last = trans.last
      val lastEvent = last.sysEvent.value
      if (!trans.exists(e => TrEnd.pattern.matcher(e.sysEvent.value).matches())) {
        val events = trans.sortBy(e => e.timestamp + e.id.value.toString)
        println()
        println("=================================================================")
        println()
        println("QUERY EXECUTING:")
        println(s"${events.last.format}")
        println()


        println("TRANSACTION START:")
        println(s"${trans.head.format}")
        println()

        val transactionEnd = LogEvents.filter(_.sysEvent === s" $trId end!!!").result.exec
        if (transactionEnd.isEmpty) {
          println("TRANSACTION NOT FINISHED")
          println()
        } else {
          val end = transactionEnd.head
          println("TRANSACTION END:")
          println(s"${end.format}")
          println()
          println(s"TRANSACTION DURATION: ${Duration.between(events.head.timestamp.value, end.timestamp.value).toMillis}ms")
        }
      }
    }
  }
  println()
  println("=================================================================")

  private def hasNoTransaction(e2: LogEvent) = {
    e2.sysEvent.value.contains("resultset.count") || e2.sysEvent.value
      .contains("DataService.getCount return:")
  }

  private def time(str: String): Timestamp = {
    Timestamp(formatter.parse(str).toInstant)
  }
}
