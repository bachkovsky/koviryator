package parser

import java.io.{File => JFile}
import java.nio.charset.StandardCharsets
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

import better.files.{File, _}
import model.LogEvent._
import model.{LogEvent, LogFile}
import utils.StringUtils._
import utils.TimeUtils._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try
import scala.util.matching.Regex

trait ParserComponent {

  val parser: LogParser

  final class LogParser {
    private val LogHeaderPattern: Regex =
      """(.*?)#(.*)\s+(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\.\d{2,3})\s\[([^\s]*)\s([^\s]*)\s?(.*)\]\s(.*?)\((.*?)\).*""".r

    def parseEvents(files: Seq[JFile]): Seq[Future[Page]] = {
      files
        .map(_.toScala)
        .map(LogFile)
        .sortBy(_.timestamp)
        .map(_.file)
        .flatMap(toEvents)
    }

    private def parse(file: File): Page = {
      def linesOf(f: File) = {
        try {
          f.lines
        } catch {
          case e: Exception =>
            try {
              println(s"file = $file was not parsed with default encoding")
              file.lines(StandardCharsets.ISO_8859_1)
            } catch {
              case _: Exception =>
                println(s"file = $file was not parsed with ISO_8859_1")
                throw e
            }
        }
      }

      val events = linesOf(file).map(parse).foldLeft(Vector[LogEvent]()) {
        case (parsedEvents, Right(event)) => parsedEvents :+ event
        case (parsedEvents :+ lastEvent, Left(unparsed)) =>
          parsedEvents :+ lastEvent.copy(
            sysEvent = lastEvent.sysEvent + "\n" + unparsed)
        case (_, Left(_)) => Vector.empty
      }
      println(s"file = $file, events count: ${events.size}")
      events
    }

    private def parse(line: String): Either[String, LogEvent] =
      line.indexOf("):") match {
        case -1 => Left(line)
        case i =>
          line.splitAt(i + 2) match {
            case (header, payload) =>
              header match {
                case LogHeaderPattern(
                    thread,
                    host,
                    Timestamp(timestamp),
                    url,
                    user,
                    ip,
                    Level(level),
                    logger
                    ) =>
                  Right(
                    LogEvent(
                      Id(0L),
                      Thread(thread),
                      Host(host),
                      timestamp,
                      Url(url),
                      User(user),
                      Ip(ip.opt.getOrElse("unknown")),
                      level,
                      Logger(logger),
                      payload
                    )
                  )
                case _ =>
                  Left(line)
              }
          }
      }

    private def toEvents(file: File): Seq[Future[Page]] = {
      println(s"process file = $file")
      file match {
        case zip if zip.extension.exists(_.contains(".zip")) =>
          extractEvents(zip)
        case log if log.extension.exists(_.contains(".log")) =>
          getEvents(List(log))
        case dir if file.isDirectory =>
          dir.children.toList.flatMap(toEvents)
        case _ => List.empty
      }
    }

    private def extractEvents(archive: File): Seq[Future[Page]] = {
      val tempDir = archive.unzip().deleteOnExit()
      getEvents(
        tempDir.list.toList.map(LogFile).sortBy(_.timestamp).map(_.file))
    }

    private def getEvents(logFiles: List[File]): Seq[Future[Page]] = {
      val outLogs = logFiles.filter(_.name.startsWith("SystemOut"))
      val errLogs = logFiles.filter(_.name.startsWith("SystemErr"))
      val logs = errLogs ++ outLogs
      logs.map(file => {
        println(s"schedule parsing = $file")
        Future {
          println(s"start parsing = $file")
          parser.parse(file)
        }
      })
    }

    private object Timestamp {
      val formatter =
        new DateTimeFormatterBuilder()
          .appendPattern("yyyy-MM-dd HH:mm:ss")
          .appendFraction(ChronoField.MILLI_OF_SECOND, 0, 3, true)
          .toFormatter

      def unapply(s: String): Option[Timestamp] =
        Try {
          LogEvent.Timestamp(formatter.parse(s).toInstant)
        }.toOption
    }
  }
}
